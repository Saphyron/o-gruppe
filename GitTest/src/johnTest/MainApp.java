package johnTest;
import java.util.ArrayList;
import java.util.List;

public class MainApp {

	public static void main(String[] args) {
		List<Person> list = List.of(new Person("Bent"), new Person("Susan"), new Person("Mikael"),
				new Person("Klaus"), new Person("Birgitte"), new Person("Liselotte"));
		List<Person> np = new ArrayList<Person>(list);
		for (int i = 0; i < np.size(); i++) {
			System.out.println(np.get(i).getName());
		}
		np.get(3).setJohn();
		System.out.println();
		for (int i = 0; i < np.size(); i++) {
			System.out.println(np.get(i).getName());
		}
		//test
	}

}
